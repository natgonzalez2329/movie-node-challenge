## README 

# MOVIE-NODE CHALLENGE 

## Índice

* [1. Descripción general](#1-descripción-general)
* [2. Implementación](#2-implementación)
* [3. Tecnologías](#3-tecnologías)

***

## 1. Descripción general
MovieNode Challenge, microservicio con 3 endpoints, en donde a través del consumo de la API http://www.omdbapi.com/, se crea otra API a través de una colección almacenada en MongoDB. En la colección de películas se almacena:

* Title
* Year
* Released
* Gender
* Director
* Actors
* Plot
* Ratings.

## 2. Implementación
## Buscador de película

A través del método GET, el buscador de película puede obtener las películas de la API, por Título de la película y especifiar el año de la misma a través del header. La información que coincide se almacena en la base de datos Mongo, el registro de la película solo ocurre una vez. Una vez almacenada, devuelve la información solicitada.

## Obtención de películas

Devuelve el total de películas almacenadas en la base de datos, a través del método GET. Si hay más de 5 películas guardadas en MongoDB, se pagina 5 películas por página, el número de la misma es determinado a través del header.

## Busca y reemplaza

En el BODY se recibe en formato JSON { movie: título de la película, find: la palabra a sustituir, replace: la palabra sustituta }, estas especificaciones modifican el Plot de la película que coincide almacenada en la base de datos. Y a través del método POST se actualiza y devuelve el valor del campo Plot de la película solicitada.
## 3. Tecnologías
- NodeJS
- Framework: KOA
- MongoDB
- ODM Mongoose
- API URL: http://www.omdbapi.com/
