const {Schema, model} = require("mongoose");
const mongoosePaginate = require('mongoose-paginate-v2');

const rmoviesSchema = new Schema({
    Title: {
        type: String
    },
    Year: {
        type: String
    },
    Released: {
        type: String
    }, 
    Genre: {
        type: String
    },  
    Director: {
        type: String
    },
    Actors: {
        type: String
    },
    Plot: {
        type: String
    },
    Ratings: {
        type: Array
    }
});

rmoviesSchema.plugin(mongoosePaginate);

module.exports = model("rmovies", rmoviesSchema)
