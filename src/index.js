require("./connectionmon.js");
const rMovies = require("./rmovies")
const Koa = require("koa");
const Router = require ("koa-router");
const fetch = require("node-fetch");
const apiKey =  "34274ef9";
const url = "http://www.omdbapi.com/";
const app = new Koa();
const router = new Router();

router.get("/:movies",  async (ctx) => {
  
  const response = await fetch(`${url}?t=${ctx.request.params.movies}&y=${ctx.header.year}&apiKey=${apiKey}`, {
    method: 'GET', 
  });
  const responseData = await response.json(); 
  const {Title, Year, Released, Genre, Director, Actors, Plot, Ratings} = responseData;
  const searchMovie = await rMovies.findOne({ Title, Year });
  if(searchMovie === null) {
    const movie = new rMovies({Title, Year, Released, Genre, Director, Actors, Plot, Ratings});
    const responseMovie = await movie.save();
    console.log("Has been saved correctly!");
    ctx.body = responseMovie;
  } else {
    console.log("Movie already exists!");
    ctx.body = searchMovie;
  }
});

router.get("/", async (ctx) => {
  const limit = Number(ctx.header.moviesbypage) || 5;
  const page = Number(ctx.header.pagination) || 1;
  const findAllMovies = await rMovies.paginate({}, { limit, page }); 
  ctx.body = findAllMovies;
});

router.post("/", async (ctx) => {
  const { movie, find, replace } = ctx.request.body;
  const year = ctx.header.year;
  const searchByYear = await rMovies.findOne({
    Title: movie,
    Year: year
  });
  if(!searchByYear) {
    ctx.status = 400,
    ctx.body = 'Movie does not exist in database';
    return ctx;
  };

  const { Plot } = searchByYear._doc;
  if(find && Plot.includes(find)) {
    try {
      const plotModified = Plot.replace(find, replace);
      const addPlot = await rMovies.updateOne(searchByYear, { Plot: plotModified });
      if(addPlot) {
        ctx.body = plotModified;
      }
    } catch (error) {
      ctx.body = error.message;
    };
  } else {
    ctx.body = `${find} not found, nothing to replace`
  };
  return ctx;
});

app.use(require("koa-body")());
app.use(router.allowedMethods());
app.use(router.routes());

app.listen(4000);
