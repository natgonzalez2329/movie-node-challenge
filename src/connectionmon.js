const mongoose = require("mongoose");

const uri = "mongodb+srv://natgonzalez:23418759ng@cluster0.6kbwp.mongodb.net/test";

mongoose.connect(uri, { 
    useNewUrlParser: true,
    useUnifiedTopology: true
});
  
mongoose.connection.once("open", _ => {
  console.log("Database is connected to", uri);
});
  
mongoose.connection.on("error", err => {
  console.log(err);
});
